# Aud-CLI-o

**Aud-CLI-o** ist ein CLI-Skript, mit welchen man Lieder von YouTube und anderen Plattformen mit dem freien und Quelloffenen Programm <a href="https://github.com/yt-dlp/yt-dlp"><strong>yt-dlp</strong></a> herunterladen kann.

##### Aud-CLI-o im Linux Terminal (XFCE)

![Aud-CLI-o im Linux Terminal (XFCE)](img/Aud-CLI-o%20Linux.png)

##### Aud-CLI-o im Windows CMD

![Aud-CLI-o im Windows CMD](img/Aud-CLI-o%20Windows.png)

## Installation

Da **Aud-CLI-o** ein Skirpt ist, installiert man dieses manuell.

### Linux:

Im Terminal gibt man folgendes ein:

```bash
git clone https://codeberg.org/Monstanner/Aud-CLI-o.git && cd Aud-CLI-o && rm LICENSE README.md && rm -r img Windows && mv Linux/* ./ && rmdir Linux && chmod u+xwr -R .git && rm -r .git
```
Nun installiert man diese benötigten Pakete mit dem Paket-Manager der verwendeten Linux Distribution:

> `yt-dlp`, `ffmpeg`, `ffprobe`

Auf Debian-basierten Distributionen gibt man dies im Terminal ein:

> `sudo apt install yt-dlp ffmpeg ffprobe`

Auf Arch-basierten Distributionen gibt man dies im Terminal ein:

> `sudo pacman -S yt-dlp ffmpeg ffprobe`

Abschließende Schritte:

* Im Dateimanager `Aud-CLI-o.sh` und `Aud-CLI-o.desktop` ausführbar machen.<br><br>
Anderfalls ein Terminal im Ordner **Aud-CLI-o** öffnen und dies eingeben > `chmod u+x Aud-CLI-o.desktop Aud-CLI-o.sh`
* Die Pfade in Aud-CLI-o.desktop ändern (Exec und Icon) zu Deinen Pfaden.<br>
(Beispiel: `/home/deinpfad/Aud-CLI-o` )
* Verschiebe `Aud-CLI-o.desktop` nach `/home/user/.local/share/applications`.
Anderfalls ein Terminal im Ordner **Aud-CLI-o** öffnen und dies eingeben > `mv Aud-CLI-o.desktop $HOME/.local/share/applications`

### Windows

Als erstes lädt man diese benötigten Pakete herunter:

 <a href="https://codeberg.org/Monstanner/Aud-CLI-o"><strong>Aud-CLI-o</strong></a>, <a href="https://github.com/yt-dlp/yt-dlp/releases"><strong>yt-dlp</strong></a>, <a href="https://www.gyan.dev/ffmpeg/builds/#release-builds"><strong>ffmpeg</strong></a>

 Folgende Pakete werden auf den Paketseiten heruntergeladen:

 #### Aud-CLI-o:

 Hier lädt man das Repository wahlweise als Zip oder als TAR.GZ herunter.

 #### yt-dlp:

`yt-dlp_win.zip`

 #### ffmpeg:

 `ffmpeg-release-full.7z`

Ist alles heruntergeladen, wird nun alles in seperaten Ordnen entpackt.

*Falls man das ffmpeg-Paket oder ein anderes Paket nicht entpacken kann, sollte <a href="https://www.7-zip.org/"><strong>7z</strong></a> weiterhelfen.*

Nachdem entpacken, gibt es nun drei Ordner. `Aud-CLI-o`, `ffmpeg` & `yt-dlp`.

Nun fügt man folgende Datein zusammen. Deswegen sollte ein primärer Ordnern erstellt werden. Zum Beispiel unter `C:\Users\DeinName\yt-dlp`.

Folgende Datein werden in den primären Ordner verschoben:

Aud-CLI-o\Windows:

> `Aud-CLI-o.bat`, `Aud-CLI-o.ico`

ffmpeg\bin:

> `ffmpeg.exe`, `ffplay.exe`, `ffprobe.exe`

yt-dlp\:

> `den gesamten Inhalt.`

Abschließende Schritte:

* Mit einen beliebigen Texteditor (Notepad, Notepad++, etc.) sucht man mit STRG+F nach `#Bitte das README.md lesen.` und ersetzt diese Zeilen mit `C:/Users/DeinName/Music/` und speichert das Skript mit STRG+S ab. (Man kann den Pfad auch individuell gestallten. Beispiele: `D:/Music/Downloads`, `C:/Users/DeinName/Downloads/Music`, etc.)
* Dann kann man von den Skript eine Verknüpfung auf den Desktop erstellen und das beigelegete Icon als Verknüfungs-Icon auswählen.

## Probleme

Da Google LLC. stetig Änderungen an YouTube vornimmt, kann es vorkommen, dass das Skript hin und wieder streikt. Das sollte mit einer einfachen und stetigen Aktualisierung von `yt-dlp` zu lösen sein:

```bash
yt-dlp -U
```
### Windows:

Aud-CLI-o öffnen, 9 eingeben, enter drücken, J eingeben, enter drücken, warten, wenn der Text erscheint, dass man eine beliebige Taste zum beenden drücken soll, macht man dies.

## Wie man Aud-CLI-o nutzt

### Linux

Aud-CLI-o sollte jetzt im Startmenü / Menü / Dash / etc. der jeweiligen Distribution aufzufinden sein. Ein klick auf das Symbol öffnet das standart Terminal. Ganz oben sieht man den Namen des Skripts und die Versionsnummer und den Autor.
Darauf folgen `Ziffern` mit Audioformaten dahinter. Und die Frage, in welchen Format das Lied heruntergeladen werden soll.

Hat man sich für ein Format entschieden, gibt man diese `Ziffer` ein und bestätigt dies dann mit `Enter`. Darauf folgt die bitte nach den Link des Liedes, welches heruntergealden werden soll. Diesen fügt man mit `STRG / CTRL + V` oder `Rechtsklick + Einfügen` ein und bestätigt das ganze wieder mit `Enter`. Dann wird das gewünschte Lied heruntergeladen. Wenn dies fertiggestellt ist, beendet sich das Skirpt automatisch. Viel Spaß beim hören von Lieder / Podcasts / etc.

### Windows

Hat man eine Verknüpfung von Aud-CLI-o erstellt, doppelklickt man auf das Symbol, so öffnet sich das Windows CMD. Ganz oben sieht man den Namen des Skripts und die Versionsnummer und den Autor. Darauf folgen `Ziffern` und `Buchstaben` mit Audioformaten oder Optionen dahinter. Und die Frage, in welchen Format das Lied heruntergeladen werden soll.

Hat man sich für ein Format entschieden, gibt man diese `Ziffer` oder `Buchstaben` ein und bestätigt dies dann mit `Enter`. Darauf folgt die bitte nach den Link des Liedes, welches heruntergealden werden soll. Diesen fügt man mit `Rechtsklick` ein und bestätigt das ganze wieder mit `Enter`. Dann wird das gewünschte Lied heruntergeladen. Wenn dies fertiggestellt ist, steht im CMD, dass man eine beliebige Taste zum beenden drücken soll. Dies macht man oder schließt es mit dem X oben Rechts. Viel Spaß beim hören von Lieder / Podcasts / etc.

## FAQ

Falls noch Fragen zu dem Skirpt vorhanden sind, hoffe ich sie hier beantworten zu können.

F: Was bedeutet CLI?<br>
A: CLI heißt Command-Line-Interface. Und ist die Konsole / das Terminal unter Linux und bei Windows ist es CMD. Also verwendet das Skript keine GUI (Graphical-User-Interfce/Grafische-Benutzer-Oberfläche).

F: Was heißt Aud-CLI-o und wie spricht man es aus?<br>
A: Was CLI bedeutet weißt Du sicher jetzt. Doch wie soll man Aud-CLI-o ausprechen? Etwa so: Aud Pause C L I Pause o? Nein. Es ist einfacher. Audio! Das CLI wurde als Indikator eingesetzt, da es "Audio" auch mit einer GUI gibt und so heißt diese Variante <a href="https://codeberg.org/Monstanner/Aud-GUI-o"><strong>Aud-GUI-o</strong></a>.

F: Darf ich das Skript forken?<br>
A: Ja, denn es ist Quelloffen und untersteht der <a href="https://codeberg.org/Monstanner/Aud-CLI-o/src/branch/main/LICENSE"><strong>GPL 3.0</strong></a>. Eine verlinkung zu diesen Skript wäre nett.

## Zukünftige Versionen Release Notes / To-Do's

###### Aud-CLI-o v.1.X XXXXXXXX

/

## Release Notes
**Anmerkung:** *KRelease = Kein Release*

###### Aud-CLI-o 1.3 20240616

* Release der Linux und Windows Version als Zip Pakete.
* ReadME.txt aus dem Linux und Windows Verzeichnis entfernt.

###### KRelease 20230618
* README.md überarbeitet.

###### Aud-CLI-o 1.2.1 20230301

* Eintrag zum aktualisieren von yt-dlp hinzugefügt. (Windows)
* `#Bitte die ReadMe.txt lesen.` zu `#Bitte das README.md lesen.` in Aud-CLI-o.bat geändert. (Windows)
* Texterklärung was man eingeben muss, wurde ergänzt. (Linux & Windows)

###### KRelease 20221227
* README.md überarbeitet und aktualisiert.

###### KRelease 20221221
* Bild von **Aud-CLI-o** im Windows CMD in main/img hochgeladen.

###### Aud-CLI-o v.1.2 20221220
* Das Skript verwendet jetzt yt-dlp anstatt youtube-dl.
* Die Prüfung ob youtube-dl installiert ist, wurde zu yt-dlp geändert.
- Installation von yt-dlp (youtube-dl) nach fehlgeschlagener Prüfung wurde entfernt.
* Die Windows Version 1.1 wurde übersprungen, um mit der Linux Version gleichauf zu sein.
* Die GitHub Links wurden zu Codeberg.org geändert.
* Neues main-Verzeichnis (img).
* Bild von **Aud-CLI-o** im Linux Terminal in main/img hochgeladen.
* README.md überarbeitet und aktualisiert.

###### KRelease 20220911

* Umzug zu codeberg.org

###### Aud-CLI-o v.1.1 20211106 (Linux)

* IF-Abfrage integriert, welche prüft ob youtube-dl installiert ist. Falls es installiert ist, macht Aud-CLI-o wie gewohnt weiter. Ist youtube-dl hingegen nicht installiert, wird dies nachgeholt. (<a href="https://github.com/Monstanner/Aud-CLI-o/commit/309a58dfc9fcca038babe52fbda71a5b9dc67c19"><strong>GitHub</strong></a>)
* Downloadverzeichnis gestzt (DOWNDIR). (<a href="https://github.com/Monstanner/Aud-CLI-o/commit/309a58dfc9fcca038babe52fbda71a5b9dc67c19"><strong>GitHub</strong></a>)
* Rechtschreibfehler in `Aud-CLI-o.desktop` verbessert. (<a href="https://github.com/Monstanner/Aud-CLI-o/commit/3b022a596c814682e73e9778b2c1350a28263d61"><strong>GitHub</strong></a>)

###### Aud-CLI-o v. 1.0 20211103 (Windows)

* Erste Veröffentlichung der Windows Version. (<a href="https://github.com/Monstanner/Aud-CLI-o/commit/f755b925b4bbbd0bbff9bb3267afee7ea4a4d78d"><strong>GitHub</strong></a>)
* Rechtschreibfehler in der Linux ReadMe.txt verbessert. (<a href="https://github.com/Monstanner/Aud-CLI-o/commit/aecb4a81136b74aca716cd8118f5e1e67a148c74"><strong>GitHub</strong></a>)

###### Aud-CLI-o v.1.0 20211009 (Linux)

* Erste Veröffentlichung der Linux Version. (<a href="https://github.com/Monstanner/Aud-CLI-o/commit/e1aa8f07a97c2b7857243ecbad3869f2d7cab699"><strong>GitHub</strong></a>)
* Link wurde ersetzt (Vorher Link zu YouTube-DL MP3: www.github.com/Monstanner/YouTube-DL-MP3. Nachher Link zu Aud-CLI-o: www.github.com/Monstanner/Aud-CLI-o). (<a href="https://github.com/Monstanner/Aud-CLI-o/commit/e1aa8f07a97c2b7857243ecbad3869f2d7cab699"><strong>GitHub</strong></a>)

## Lizenz

Autor: Marcel (Monstanner) S.

Lizenz: GPL 3.0

Repository: https://codeberg.org/Monstanner/Aud-CLI-o

## Kontakt

<a href="https://mastodon.social/@monstanner"><strong>Mastodon</strong></a> <br>
<a href="mailto:monstanner@gmail.com"><strong>E-Mail</strong></a>
